package com.infantaelena.chatclient;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * @author Brahyan Marin Pulgarin
 * Esta actividad solo se muestra al usuario la primera vez que accede a la aplicación, en ella se
 * selecciona el nick con el que se conectara a la aplicacion, una vez elegido este nick se guarda
 * en la memoria interna del teléfono y se utilizará en posteriores conexiones automaticamente.
 */

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{

    private Button btnRegistro;
    private EditText etNickName;

    /**
     * Si
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Se para el hilo para que pueda ver la splash screen, si no se hace carga tan rapido que
        //no se llega a mostrar
        try {
            Thread.sleep(1000);
        }catch (Exception e){}
        setTheme(R.style.Theme_ChatClient);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btnRegistro = findViewById(R.id.btnRegistro);
        etNickName = findViewById(R.id.etNickName);

        if(getSharedPreferences(Constantes.SHAREDPREFS_FILE,MODE_PRIVATE).
                getString(Constantes.SHAREDPREFS_NICK,null) != null){
            cambiarActividadPrincipal();
        }

        btnRegistro.setOnClickListener(this);

    }

    /**
     * Si el nickname es valido se cambia de actividad y se guarda el nick en las preferencias
     * compartidas, en caso contrario se muestra mensaje de error.
     * @param view el boton pulsado
     */
    @Override
    public void onClick(View view) {

        String nickname = etNickName.getText().toString().trim();

        if( nickname.equalsIgnoreCase(Constantes.GLOBAL_CHAT_NAME) ){
            mostrarMensajeError("No puede usarse ese nickname");
        }else if( nickname.length() < 3 ){
            mostrarMensajeError("El nick debe tener al menos 3 caracteres");
        }else{
            guardarNickName(nickname);
            cambiarActividadPrincipal();
        }

    }

    /**
     * Guarda el nickname utilizado por el usuario en el almacenamiento interno para usarlo en
     * posteriores conexiones.
     */
    private void guardarNickName(String nickname){
        SharedPreferences prefs = getSharedPreferences(Constantes.SHAREDPREFS_FILE,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Constantes.SHAREDPREFS_NICK,nickname);
        editor.commit();
    }

    /**
     * Muestra mensaje toast si selecciona un nombre de usuario no valido.
     * @param mensaje
     */
    private void mostrarMensajeError(String mensaje){
        Toast.makeText(this,mensaje,Toast.LENGTH_SHORT).show();
    }

    /**
     * Incicia la actividade principal de la actividad.
     */
    private void cambiarActividadPrincipal(){
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
        this.finish();
    }
}