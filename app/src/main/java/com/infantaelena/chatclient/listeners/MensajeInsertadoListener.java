package com.infantaelena.chatclient.listeners;

import com.infantaelena.chatclient.database.model.Mensaje;

import java.util.EventListener;

/**
 * @author Brahyan Marin
 *Interfaz que han de implementar los suscritos a las notificaciones de insercion de Mensajes
 */
public interface MensajeInsertadoListener extends EventListener {
    void MensajeInsertado(Mensaje mensajeInsertado);
}
