package com.infantaelena.chatclient.listeners;

import com.infantaelena.chatclient.database.model.Contacto;

import java.util.EventListener;

/**
 * @author Brahyan Marin
 *Interfaz que han de implementar los suscritos a las notificaciones de insercion de Contactos
 */

public interface ContactoInsertadoListener extends EventListener {
    void ContactoInsertado(Contacto contactoInsertado);
}
