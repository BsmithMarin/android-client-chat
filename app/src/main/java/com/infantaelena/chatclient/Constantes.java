package com.infantaelena.chatclient;

/**
 * @author Brahyan Marin Pulgarin
 *
 * Clase que define constantes utilizadas en toda la aplicación, como la ip y puerto del servidor,
 * la etiqueta a usar en los logs de la aplicación, o los tipos de csv que se pueden recibir y enviar
 * al servidor, entre otros.
 */

public class Constantes {

    private Constantes(){}

    public final static String IP_SERVIDOR = "192.168.1.51";
    public final static int PUERTO_SERVIDOR = 9000;

    public final static String APP_LOG_TAG = "chatapp";

    public final static String GLOBAL_CHAT_NAME = "GLOBAL";

    public final static String SHAREDPREFS_FILE = "credenciales";
    public final static String SHAREDPREFS_NICK = "nickname";

    public static class TipoPaquete {
        public final static String NUEVA_CONEXION = "NUEVA_CONEXION";
        public final static String MENSAJE = "MENSAJE";
        public final static String LISTA_CONECTADOS = "LISTA_CONECTADOS";
    }

}
