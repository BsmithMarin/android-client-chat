package com.infantaelena.chatclient.chatfragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.infantaelena.chatclient.Constantes;
import com.infantaelena.chatclient.R;
import com.infantaelena.chatclient.adapters.MensajesAdapter;
import com.infantaelena.chatclient.database.DbManager;
import com.infantaelena.chatclient.database.model.Contacto;
import com.infantaelena.chatclient.database.model.Mensaje;
import com.infantaelena.chatclient.listeners.MensajeInsertadoListener;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import server.comunicacion.ServerConnectionHolder;

/**
 * @author Brahyan Marin Pulgarin
 * Fragmento que muestra la conversacion con un contacto en particular, envia mensajes al
 * contacto y actualiza la vista a medida que los mensajes se introducen en la base de datos.
 */

public class ChatFragment extends Fragment implements MensajeInsertadoListener,View.OnClickListener {

    RecyclerView rvMensajes;

    EditText etMensaje;

    Button btnEnviarMensaje;

    MensajesAdapter mensajesAdapter;

    List<Mensaje> listaMensajes;

    Contacto contacto;

    DbManager db;

    public ChatFragment(DbManager dbManager, Contacto contacto) {
        db = dbManager;
        this.contacto = contacto;
        db.addMensajeListener(this);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //Cambia el titulo al nombre del contacto
        getActivity().setTitle(contacto.nickname);
        return inflater.inflate(R.layout.fragment_chat, container, false);
    }

    @Override
    public void onViewCreated(View view,Bundle savedInstantes){

        cargarMensajes();
        rvMensajes = view.findViewById(R.id.rvMensajes);

        etMensaje =view.findViewById(R.id.etMensaje);
        btnEnviarMensaje = view.findViewById(R.id.btnEnviarMensaje);

        btnEnviarMensaje.setOnClickListener(this);
    }

    /**
     * Carga los mensajes que ya puediese haber en la base de datos con este contacto
     */
    private void cargarMensajes(){
        listaMensajes = new ArrayList<>();
        new Thread(()->{
            listaMensajes.addAll(db.getMensajesByContacto(contacto));
            mensajesAdapter = new MensajesAdapter(listaMensajes);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(requireContext());

            getActivity().runOnUiThread(()->{
                rvMensajes.setLayoutManager(linearLayoutManager);
                rvMensajes.setAdapter(mensajesAdapter);
                rvMensajes.scrollToPosition(listaMensajes.size()-1);
            });
        }).start();
    }

    /**
     * Callback ejecutado cada vez que un mensaje es insertado en la base de datos,
     * actualiza la vista para mostrar el nuevo mensaje
     * @param mensajeInsertado
     */
    @Override
    public synchronized void MensajeInsertado(Mensaje mensajeInsertado) {

        if(mensajeInsertado.idDeContacto == contacto.idcontacto){
            getActivity().runOnUiThread(()->{
                listaMensajes.add(mensajeInsertado);
                mensajesAdapter.notifyItemInserted(listaMensajes.size()-1);
                rvMensajes.scrollToPosition(listaMensajes.size()-1);
            });
        }
    }

    /**
     * Elimina de la lista de listeners de mensaje insertado el fragmento actual y vuelve
     * a poner el titulo por defecto a la apliacion.
     */
    @Override
    public void onDestroy(){
        getActivity().setTitle("Chat Client");
        db.removeMensajeListener(this);
        super.onDestroy();
    }

    /**
     * Utiliza el buffer de salida del socket para enviar mensaje al cliente, esto se realiza
     * en un nuevo hilo para no bloquear el hilo principal.
     * @param view
     */
    @Override
    public void onClick(View view) {

        String textoMensaje = etMensaje.getText().toString();

        if(textoMensaje.length()<1) //No se pueden mandar mensajes vacios
            return;

        new Thread(()->{
            try{
                String paqueteMensaje = ServerConnectionHolder.generateCsvRow(new String[]{
                        Constantes.TipoPaquete.MENSAJE,
                        contacto.nickname,
                        textoMensaje
                });

                ServerConnectionHolder.writeLine(paqueteMensaje);
                getActivity().runOnUiThread(()->etMensaje.setText("")); //Limpia el campo de escritura
                db.insertarMensaje(new Mensaje(
                        contacto.idcontacto,
                        textoMensaje,
                        false));
            }catch (IOException e){
                Log.d(Constantes.APP_LOG_TAG,"No se pudo mandar el mensaje",e);
                ServerConnectionHolder.getServerConnection();//Trata de reestablecer conexion
                getActivity().runOnUiThread(()->
                        Toast.makeText(getContext(),"No se pudo mandar el mensaje",Toast.LENGTH_SHORT)
                        .show());
            }

        }).start();


    }
}