package com.infantaelena.chatclient.chatfragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.infantaelena.chatclient.R;
import com.infantaelena.chatclient.adapters.ContactsAdapter;
import com.infantaelena.chatclient.database.DbManager;
import com.infantaelena.chatclient.database.model.Contacto;
import com.infantaelena.chatclient.listeners.ContactoInsertadoListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Fragmento que muestra las conversaciones del usuario
 */
public class ContactosFragment extends Fragment implements ContactoInsertadoListener {

    DbManager db;

    RecyclerView rvContactos;
    ContactsAdapter contactsAdapter;

    List<Contacto> listaContactos;

    public ContactosFragment( DbManager dbManager) {
        this.db = dbManager;
        db.addContactoListener(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_contactos, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstances){

        rvContactos = view.findViewById(R.id.rvContactos);
        cargarListaContactos();
    }

    /**
     * Carga los contactos que ya estan en la base de datos
     */
    private void cargarListaContactos(){
        listaContactos = new ArrayList<>();
        contactsAdapter = new ContactsAdapter(listaContactos);
        new Thread(()->{
            //Creacion de lista de contactos
            listaContactos.addAll(db.getAllContactos());
            LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvContactos.getContext(),
                    layoutManager.getOrientation());
            getActivity().runOnUiThread(()->{
                rvContactos.setAdapter(contactsAdapter);
                rvContactos.addItemDecoration(dividerItemDecoration);
                rvContactos.setLayoutManager(layoutManager);
            });

        }).start();

    }

    /**
     * Actualiza la vista con el contacto insertado
     * @param contactoInsertado
     */
    @Override
    public synchronized void ContactoInsertado(Contacto contactoInsertado) {
        getActivity().runOnUiThread(()->{
            listaContactos.add(contactoInsertado);
            contactsAdapter.notifyItemInserted(listaContactos.size()-1);
        });
    }

    /**
     * revoca suscripción a notificaciones de insercion de contacto al finalizar el fragmento
     */
    @Override
    public void onDestroy(){
        db.removeContactoListener(this);
        super.onDestroy();
    }
}