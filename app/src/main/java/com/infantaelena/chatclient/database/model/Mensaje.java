package com.infantaelena.chatclient.database.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

/**
 * @author Brahyan Marin
 * Entidad mensaje, representa los mensajes enviados y recibidos con los diferentes usuarios
 */

@Entity(tableName = "mensaje",indices = {@Index("id_contacto")},
        foreignKeys = {@ForeignKey(entity = Contacto.class,
                parentColumns = "idcontacto",
                childColumns = "id_contacto",
                onDelete = ForeignKey.CASCADE)})
public class Mensaje {

    @PrimaryKey(autoGenerate = true)
    public long idmensaje;

    @ColumnInfo(name = "id_contacto")
    public long idDeContacto;

    @ColumnInfo(name = "texto")
    @NonNull
    public String texto;

    @ColumnInfo(name= "recibido")
    public boolean recibido;

    public Mensaje(long idDeContacto, @NonNull String mensaje, boolean recibido){
        this.idDeContacto = idDeContacto;
        texto = mensaje;
        this.recibido = recibido;
    }

    public Mensaje(){}

    @Override
    public String toString() {
        return "Mensaje{" +
                "idmensaje=" + idmensaje +
                ", idDeContacto=" + idDeContacto +
                ", texto='" + texto + '\'' +
                ", recibido=" + recibido +
                '}';
    }
}
