package com.infantaelena.chatclient.database.model;

import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.Relation;

import java.util.List;

/**
 * @author Brahyan Marin
 * Entidad que representa la relacion de uno a varios entre Contactos y Mensajes
 */

public class ContactoConMensajes {

    @Embedded public Contacto contacto;

    @Relation(
            parentColumn = "idcontacto",
            entityColumn = "id_contacto"
    )

    public List<Mensaje> mensajes;


}
