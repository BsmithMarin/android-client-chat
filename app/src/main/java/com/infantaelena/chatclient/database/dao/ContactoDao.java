package com.infantaelena.chatclient.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;

import com.infantaelena.chatclient.database.model.Contacto;
import com.infantaelena.chatclient.database.model.ContactoConMensajes;

import java.util.List;

/**
 * @author Brahyan Marin
 * Interfaz que permite recuperar entidades de tipo Contacto de la base de datos
 */

@Dao
public interface ContactoDao {

    @Insert
    long insertContacto(Contacto contacto);

    @Transaction
    @Query("SELECT * FROM contacto WHERE nickname=:nickname")
    ContactoConMensajes getUsuarioConMensajes(String nickname);

    @Query("SELECT * FROM contacto")
    List<Contacto> getAllContactos();

    @Query("SELECT * FROM contacto where nickname=:nickname")
    Contacto getContactoByNickname(String nickname);

}
