package com.infantaelena.chatclient.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.infantaelena.chatclient.database.model.Mensaje;

import java.util.List;

/**
 * @author Brahyan Marin
 * Interfaz que permite realizar operaciones CRUD con la entidad Mensaje
 */

@Dao
public interface MensajeDao {

    @Insert
    long insertMensaje(Mensaje mensaje);

    @Query("SELECT * FROM mensaje WHERE id_contacto = " +
            "(SELECT idcontacto FROM contacto where nickname=:nickname)" +
            "ORDER BY idmensaje")
    List<Mensaje> getMensajesByNickName(String nickname);
}
