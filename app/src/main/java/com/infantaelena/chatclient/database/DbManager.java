package com.infantaelena.chatclient.database;

import android.content.Context;
import android.util.Log;

import com.infantaelena.chatclient.Constantes;
import com.infantaelena.chatclient.database.model.Contacto;
import com.infantaelena.chatclient.database.model.Mensaje;
import com.infantaelena.chatclient.database.util.ChatDataBaseUtil;
import com.infantaelena.chatclient.database.util.ChatDatabase;
import com.infantaelena.chatclient.listeners.ContactoInsertadoListener;
import com.infantaelena.chatclient.listeners.MensajeInsertadoListener;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Brahyan Marin
 * Clase que gestiona la manipulación de la base de datos y sus entidades, asi como la
 * notificación de los cambios en la BD a las clases que se hayan suscrito a las notificaciones.
 * Usa patron singleton para asegurar que la instancia es la misma en toda la aplicación.
 */

public class DbManager {

    private final ChatDatabase db;

    //Listeners de las notificaciones de insercion
    private static final List<ContactoInsertadoListener> listenersContacto = new ArrayList<>();
    private static final List<MensajeInsertadoListener> listenersMensaje = new ArrayList<>();

    private static DbManager dbManager;

    private DbManager(Context context){
        db = ChatDataBaseUtil.getChatDatabase(context);
    }

    /**
     * Devuelve instacia del gestor de base de datos con el contexto dado
     * @param context
     * @return
     */
    public static DbManager getInstance(Context context){

        if(dbManager == null){
            dbManager = new DbManager(context);
        }
        return dbManager;
    }

    /**
     * Inserta contacto en la BD si no existe ya
     * @param contacto
     */
    public synchronized void insertarContactoSiNOExiste(Contacto contacto){

        try{
            if( db.contactoDao().getContactoByNickname(contacto.nickname) == null ){
                long id = db.contactoDao().insertContacto(contacto);
                contacto.idcontacto = id;
                notificarNuevoContacto(contacto);
            }
        }catch (Exception e){
            Log.d(Constantes.APP_LOG_TAG,"ERROR AL INSERTAR CONTACTO EN LA BD",e);
        }
    }

    /**
     * Inseta mensaje en la base de datos
     * @param mensaje
     */
    public synchronized void insertarMensaje(Mensaje mensaje){

        try{
            mensaje.idmensaje = db.mensajeDao().insertMensaje(mensaje);
            notificarNuevoMensaje(mensaje);
        }catch (Exception e ){
            Log.d("BD","ERROR AL INSERTAR MENSAJE EN LA BD",e);
        }
    }

    public synchronized long getContactoIdByNickname(String nickname){

        return db.contactoDao().getContactoByNickname(nickname).idcontacto;
    }

    public List<Contacto> getAllContactos(){
        return db.contactoDao().getAllContactos();
    }


    public synchronized List<Mensaje> getMensajesByContacto(Contacto contacto){
        return db.contactoDao().getUsuarioConMensajes(contacto.nickname).mensajes;
    }


    //----------GESTION DE LISTENERS--------------

    public void addContactoListener(ContactoInsertadoListener listener){
        listenersContacto.add(listener);
    }

    public void addMensajeListener(MensajeInsertadoListener listener){
        listenersMensaje.add(listener);
    }

    public void removeContactoListener(ContactoInsertadoListener listener){
        listenersContacto.remove(listener);
    }

    public void removeMensajeListener(MensajeInsertadoListener listener){
        listenersMensaje.remove(listener);
    }

    private synchronized void notificarNuevoMensaje(Mensaje mensaje){
        listenersMensaje.forEach((listener)->{
            listener.MensajeInsertado(mensaje);
        });
    }

    private synchronized void notificarNuevoContacto(Contacto contacto){
        listenersContacto.forEach((listener)->{
            listener.ContactoInsertado(contacto);
        });
    }
}
