package com.infantaelena.chatclient.database.util;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.infantaelena.chatclient.database.dao.ContactoDao;
import com.infantaelena.chatclient.database.dao.MensajeDao;
import com.infantaelena.chatclient.database.model.Contacto;
import com.infantaelena.chatclient.database.model.ContactoConMensajes;
import com.infantaelena.chatclient.database.model.Mensaje;

/**
 * @author Brahyan Marin Pulgarin
 * Base de datos generada por Room a partir de las entidades y DAOs dados
 */

@Database(entities = {Contacto.class, Mensaje.class} ,version = 1)
public abstract class ChatDatabase extends RoomDatabase {

    public abstract ContactoDao contactoDao();

    public abstract MensajeDao mensajeDao();

}
