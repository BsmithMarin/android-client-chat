package com.infantaelena.chatclient.database.util;

import android.content.Context;

import androidx.room.Room;

/**
 * @author Brahyan Marin
 * Crea instancia de la base datos usando patron singleton
 */
public class ChatDataBaseUtil {

    private static ChatDatabase chatDatabase;


    private ChatDataBaseUtil(){}

    public static ChatDatabase getChatDatabase(Context context){

        if(chatDatabase == null){
            chatDatabase = Room.databaseBuilder(context,
                    ChatDatabase.class,
                    "chatdb").build();
        }

        return chatDatabase;
    }

}
