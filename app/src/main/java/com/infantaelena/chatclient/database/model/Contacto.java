package com.infantaelena.chatclient.database.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

/**
 * @author Brahyan Marin
 * Entidad contacto de la BD, representa los usuarios de la apliación
 */

@Entity(tableName = "contacto",
        indices = {@Index(value = {"nickname"}, unique = true)}
)
public class Contacto {

    @PrimaryKey(autoGenerate = true)
    public long idcontacto;

    @ColumnInfo(name = "nickname")
    public String nickname;

    public Contacto (String nick){
        nickname = nick;
    }
    public Contacto (){

    }

    @Override
    public String toString() {
        return "Contacto{" +
                "idcontacto=" + idcontacto +
                ", nickname='" + nickname + '\'' +
                '}';
    }
}
