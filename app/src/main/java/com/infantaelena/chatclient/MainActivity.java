package com.infantaelena.chatclient;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.infantaelena.chatclient.chatfragments.ContactosFragment;
import com.infantaelena.chatclient.database.DbManager;
import com.infantaelena.chatclient.database.model.Contacto;
import com.infantaelena.chatclient.database.model.Mensaje;

import java.io.BufferedReader;
import java.io.IOException;

import server.comunicacion.ServerConnectionHolder;

/**
 * @author Brahyan Marin Pulgarin
 *
 * Actividad principal de la aplicación, se encarga de recibir los paquetes que llegan del servidor.
 * Es anfritriona de dos fragmentos, uno para mostrar la lista de contactos, y otro para las
 * conversaciones con esos contactos.
 */
public class MainActivity extends AppCompatActivity implements Runnable{

    DbManager db;

    String nikname;

    Thread hiloIniciaBd,hiloServidor;

    /**
     * Inicia el gestor de base de datos y se lo pasa al fragmento de contactos para que recupere
     * los contactos
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Inicial el gestor de base de base de datos y el fragmento que muestra contactos
        hiloIniciaBd = new Thread(()->{
            db = DbManager.getInstance(this);
            db.insertarContactoSiNOExiste(new Contacto(Constantes.GLOBAL_CHAT_NAME));
            runOnUiThread(()->{
                //Inicia el fragmento que muestra las conversaciones abiertas
                ContactosFragment contactosFragment = new ContactosFragment(db);
                getSupportFragmentManager().beginTransaction().
                        replace(R.id.fragmentContainer, contactosFragment).
                        commit();
            });

        });
        hiloIniciaBd.start();

        nikname = (getSharedPreferences(Constantes.SHAREDPREFS_FILE,Context.MODE_PRIVATE)).
                getString(Constantes.SHAREDPREFS_NICK,"defaultUser");

        escucharServidor();
    }

    /**
     * Inicia nuevo hilo para recibir paquetes desde el servidor y guarda la información llegada del
     * servidor en la base de datos, y esta a su vez notifica a los fragmentos suscritos a las
     * notificaciones de inserción de datos para que actualicen sus vistas.
     */
    private void escucharServidor() {

        hiloServidor = new Thread(this);
        hiloServidor.start();
    }

    @Override
    public void run(){
        //Iniciliza conexion con el servidor
        ServerConnectionHolder.getServerConnection();

        try{
            //Informa de nueva conexion
            ServerConnectionHolder.writeLine(
                    ServerConnectionHolder.generateCsvRow( new String[] {
                            Constantes.TipoPaquete.NUEVA_CONEXION,
                            nikname
                    })
            );
            //Espera a que la base de datos este iniciada
            hiloIniciaBd.join();
            //Buffer para lectura de paquetes
            BufferedReader br = ServerConnectionHolder.reader;
            String lineaRecibida;
            while ( (lineaRecibida = br.readLine()) != null ){

                Log.d(Constantes.APP_LOG_TAG,"***LINEA RECIBIDA: "+lineaRecibida);
                //Argumentos del paquete
                String[] arguments = lineaRecibida.split(";");

                switch( arguments[0] ){
                    //Inserta mensaje en la base de datos, y el usuario si no estubiese ya
                    case Constantes.TipoPaquete.MENSAJE :
                        new Thread(()->{
                            db.insertarContactoSiNOExiste( new Contacto(arguments[1]) );
                            db.insertarMensaje(new Mensaje(
                                    db.getContactoIdByNickname(arguments[1]),
                                    arguments[2],
                                    true));
                        }).start();
                        break;
                    //Si se conecta alguien nuevo al chat se añade a lista de contactos
                    case Constantes.TipoPaquete.NUEVA_CONEXION:
                        new Thread(()->{
                            db.insertarContactoSiNOExiste(new Contacto( arguments[1] )) ;
                        }).start();
                        break;
                    //Se añaden todos aquellos usuarios que ya estuvieses conectados cuando se
                    //conecta el usuario.
                    case Constantes.TipoPaquete.LISTA_CONECTADOS:
                        new Thread(()->{
                            for(int i=1;i<arguments.length;i++ )
                                db.insertarContactoSiNOExiste(new Contacto(arguments[i]));
                        }).start();
                        break;
                }
            }
        } catch (IOException e) {
            Log.d(Constantes.APP_LOG_TAG, "Error de conexion con el servidor", e);
        }catch (NullPointerException e){
            Log.d(Constantes.APP_LOG_TAG,"STREAM NULO CON EL SOCKET",e);
        }catch (InterruptedException e){
            Log.d(Constantes.APP_LOG_TAG,"HILO INTERRUMPIDO",e);
        }

    }

}