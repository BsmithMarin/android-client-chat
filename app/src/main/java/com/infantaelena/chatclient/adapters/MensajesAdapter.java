package com.infantaelena.chatclient.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.infantaelena.chatclient.R;
import com.infantaelena.chatclient.database.model.Mensaje;

import java.util.List;

/**
 * @author Brahyan Marin Pulgarin
 * Adaptador que une los datos de los mensajes de una conversacion a la vista de ese mensaje.
 */

public class MensajesAdapter extends RecyclerView.Adapter {

    private static final int VIEW_TYPE_MENSAJE_ENVIADO = 1;
    private static final int VIEW_TYPE_MENSAJE_RECIBIDO = 2;

    List<Mensaje> listaMensajes;

    public MensajesAdapter(List<Mensaje> mensajes){
        listaMensajes = mensajes;
    }


    private static class MensajeEnviadoHolder extends RecyclerView.ViewHolder {

        TextView tvMensaje;

        public MensajeEnviadoHolder(@NonNull View itemView) {
            super(itemView);

            tvMensaje = itemView.findViewById(R.id.tvTextoMensajeEnviado);
        }
        void bind(Mensaje mensaje){
            tvMensaje.setText(mensaje.texto);
        }
    }

    private static class MensajeRecibidoHolder extends RecyclerView.ViewHolder {

        TextView tvMensaje;

        public MensajeRecibidoHolder(@NonNull View itemView) {
            super(itemView);

            tvMensaje = itemView.findViewById(R.id.tvTextoMensajeRecibido);
        }
        void bind(Mensaje mensaje){
            tvMensaje.setText(mensaje.texto);
        }
    }

    @Override
    public int getItemViewType(int position) {

        if(listaMensajes.get(position).recibido){
            return VIEW_TYPE_MENSAJE_RECIBIDO;
        }else{
            return VIEW_TYPE_MENSAJE_ENVIADO;
        }
    }


    @NonNull
    @Override
    public synchronized RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        if (viewType == VIEW_TYPE_MENSAJE_ENVIADO){
            View view = LayoutInflater.from(parent.getContext()).
                    inflate(R.layout.mensaje_enviado_cv,parent,false);

            return new MensajeEnviadoHolder(view);
        }else if(viewType == VIEW_TYPE_MENSAJE_RECIBIDO){
            View view = LayoutInflater.from(parent.getContext()).
                    inflate(R.layout.mensaje_recibido_cv,parent,false);
            return new MensajeRecibidoHolder(view);
        }else {
            return null;
        }

    }


    /**
     * Asocia la vista de un mensaje a cada mensaje recuperado de la BD, cambiando color y
     * orientación, en función de si se trata de un mensaje recibido o enviado.
     * @param holder
     * @param position
     */
    @Override
    public synchronized void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if( holder.getItemViewType() == VIEW_TYPE_MENSAJE_ENVIADO ){
            ((MensajeEnviadoHolder)holder).bind(listaMensajes.get(position));
        }else{
            ((MensajeRecibidoHolder)holder).bind(listaMensajes.get(position));
        }

    }

    @Override
    public synchronized int getItemCount() {
        return listaMensajes.size();
    }

}
