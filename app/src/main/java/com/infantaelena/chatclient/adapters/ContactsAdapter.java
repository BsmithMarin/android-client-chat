package com.infantaelena.chatclient.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.infantaelena.chatclient.Constantes;
import com.infantaelena.chatclient.R;
import com.infantaelena.chatclient.chatfragments.ChatFragment;
import com.infantaelena.chatclient.database.DbManager;
import com.infantaelena.chatclient.database.model.Contacto;


import java.util.List;

/**
 * @author Brahyan Marin Pulgarin
 * Adaptador que de forma dinamina genera las CardView que se usan para representar los
 * contactos de la base de datos
 */

public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ViewHolder> {

    private List<Contacto> contactos;

    public ContactsAdapter(List<Contacto> listaContactos){
        contactos = listaContactos;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        final TextView tvContactName;

        final ImageView ivImagenContacto;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvContactName = itemView.findViewById(R.id.tvContactName);
            ivImagenContacto = itemView.findViewById(R.id.ivImagenContacto);
        }

    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View cardView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.contact_card_view,parent,false);

        return new ViewHolder(cardView);
    }

    /**
     * Une los datos a la vista, además establece listener de clicks a toda la tarjeta para que
     * abrir el fragmento de chat con el contacto seleccionado
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.tvContactName.setText( contactos.get(position).nickname );

        if(contactos.get(position).nickname.equals(Constantes.GLOBAL_CHAT_NAME)){
            holder.ivImagenContacto.setImageResource(R.drawable.globe);
        }

        holder.itemView.setOnClickListener((click)->{
            ChatFragment fragment = new ChatFragment(
                    DbManager.getInstance(click.getContext()),
                    contactos.get(position));

            AppCompatActivity activity = (AppCompatActivity) click.getContext();
            activity.getSupportFragmentManager().
                    beginTransaction().
                    replace(R.id.fragmentContainer,fragment).
                    addToBackStack(null).
                    commit();
        });
    }

    @Override
    public int getItemCount() {
        return contactos.size();
    }



}
