package server.comunicacion;

import android.util.Log;

import com.infantaelena.chatclient.Constantes;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

/**
 * @author Brahyan Marin Pulgarin
 * Clase que actua de contenedor para la conexion con el servidor para toda la aplicación.
 */

public class ServerConnectionHolder {

    private static Socket serverConnection;

    private static BufferedWriter writer;

    public static BufferedReader reader;

    private ServerConnectionHolder(){
    }

    public synchronized static Socket getServerConnection(){

        if(serverConnection == null){
            iniciarConexion();
        }else if (serverConnection.isClosed()){
            iniciarConexion();
        }

        return serverConnection.isConnected()?serverConnection:null;
    }

    /**
     * Escribe linea en el stream de salida del socket
     * @param linea linea de texto a escribir
     */
    public synchronized static void writeLine(String linea) throws IOException{
        writer.write(linea);
        writer.newLine();
        writer.flush();
    }

    /**
     * Inicia conexion con el servidor y crea buffers de escritura y lectura
     */
    private static void iniciarConexion(){
        try {
            serverConnection = new Socket(Constantes.IP_SERVIDOR,Constantes.PUERTO_SERVIDOR);
            writer = new BufferedWriter( new OutputStreamWriter( serverConnection.getOutputStream()));
            reader = new BufferedReader( new InputStreamReader( serverConnection.getInputStream()));
        } catch (IOException e) {
            Log.d(Constantes.APP_LOG_TAG,"ERROR AL CREAR SOCKET CON EL SERVIDOR,",e);
        }
    }

    /**
     * Genenera una linea en formato csv a partir de los argumentos dadoss
     * @param columnas columnas de la fila
     * @return linea csv
     */
    public static String generateCsvRow(String[] columnas){

        StringBuilder sb = new StringBuilder("");

        for(String columna : columnas){
            sb.append(columna);
            sb.append(";");
        }

        return sb.toString();
    }
}
